# Overview

This documents provides an example App to show how the dependency management works.

![](sample-project-structure.jpg)

The example is an app (imaginatively, called App). It uses two frameworks, Content and Utility. Content framework itself uses Utility as well. In addition, it also links to a Trig framework which App does not directly depend on.

App and Utility also uses Tester framework in their unit tests, which in turn uses TesterCore.

Note that implementation-wise these repos only have a few simple functions. What’s important is how they’re linked.

# Main target dependnecies

This is how the various frameworks manage dependnecies:

* Trig and Utility have no public dependencies, so they don’t need any particular setup.
* Content uses both Trig and Utility directly, so it mentions both of them in its `Cartfile`. In addition, its project has a reference to these as sub-projects and links to their framework products.
* App directly uses Content and Utility, so it mentions these in its `Cartfile`. Trig is not a directly dependency, so it’s not mentioned in `Cartfile`. Still, it *is* checked out, added to the app project and linked against. Note that all three of these frameworks are also embedded in App.

# Test target dependencies

This is how the projects related to testing are set up:

* Tester doesn’t use TesterCore for *its own* testing, rather it augments it to provide new testing API. As such, it adds Tester core as a *public* dependancy (in `Cartfile`).
* Utility and App use Tester, but only during testing. Since Tester is not required for building the main target, they include Tester as a *private* dependancy (in `Cartfile.private`).

You may notice that when working on Content repo, it won’t check out either of Tester or TesterCore, as it does’t need to. That said, if you wanted to be able to run Utility’s unit tests from Content’s repo, you would still have to add Tester and TesterCore.
