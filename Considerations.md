
## Automation

It should be possible to fully automate build of any repository, regardless of whether they are apps or frameworks, on a specific machine (as long as they have the permission to access everything they need).

*Automatable* is distingished *automated* in this context to emphesasis the fact that whether the process is done automatically or not depends on the context.

As an example, an individual developer could clone the project normally, run some command line tools, and then use Xcode to build the project. Meanwhile, a CI server would clone, prepare, and build everything automatically.

Based on this requirement, for example, it’s unacceptable to require a framework file to be sourced, built, and added to the project manually.

## Uniformity

Uniformity of structure between different projects helps reduces the mental overhead of switching between different projects.

The requirement for automation enforces a certain level of uniformity. We also use strong conventions to increase uniformity across projects.

This documents recommends conventions on the structure of repo and projects. [Swift Style Guide](https://github.com/RGBCO/SwiftStyleGuide) focuses on conventions applied to the source files.

## Customisability

Automation and Uniformity are important make it easier to jump between projects. At the same time, we acknowledge the importance of custom behaviour for specific projects.

As an example, consider an app with large assets. Automation suggests it should be possible to build this project from anywhere. So the assets might live in a submodule on a git server. However, pushing to and pulling from huge repos is inconvenient during development. With that in mind, the developers working on the project could work against a repo or content server in the local network on a daily basis.

## Swift

Currently, [Swift compiler doesn’t ensure binary compatiblity between different versions](https://developer.apple.com/swift/blog/?id=2). While frameworks built in one version *might* work with a different version, this isn’t guaranteed, and might manifest as crashes or unexpected bugs in the app.

As such, *Swift binaries must not be commited to repositories*. In addition, while technically frameworks *can* be build independently (using the same compiler version) and then added to the project, ideally they’d be built directly by Xcode when preparing the final project.

## Ease of use

Both adding dependencies and ensuring a framework can be used as a dependency should be as easy as possible.

This factor is very important when considering which package manager to use: CocoaPods require creating configuration files just for the sake of working with CocoaPods and it creates a completely new project for building frameworks. In contrast, Carthage only needs a shared scheme for building the target.
