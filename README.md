* [Considerations](Considerations.md) goes through the rationale behind the suggested approach.
* [Approach](Approach.md) details how the dependencies should be managed.
* [Sample](Sample.md) shows how dependency is used in sample app.
