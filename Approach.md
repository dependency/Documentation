# Overview

We suggest that dependency between different repositories should be managed using Carthage. However, Carthage should not actually *build* the dependencies. Instead, the projects are added to the parent project and build by Xcode itself (as part of building the final target).

# Preparing dependencies

## Internal dependencies

Strictly speaking, *any* project can be used as a dependency without any change. That’s because use Carthage only to manage checkouts and not to actually build. However, it’s strongly recomended the projects are set up as following:

* Repos should have a separate workspace. The workspace is used when working directly on the repo and should keep anything private to the project (e.g. specialised schemes, test projects, etc.).
* The main project should have a shared scheme for building the main artefact (make sure the shared scheme is on the project, not the workspace).
* Use semantic versioning to tag stable versions if appropriate.

## External dependencies

External dependencies must be forked before being linked. This is to ensure that the repo doesn’t unexpectedly change (e.g. the main publisher doesn’t remove it).

# Linking to dependencies

## Preparing git

Before adding any dependencies, make sure that git ignores the folder `Carthage`. None of the artefacts created by carthage should be added to the repo.

## Adding dependencies

Dependencies should be added by creating `Cartfile` or `Cartfile.private` files. See [Carthage docs](https://github.com/Carthage/Carthage/blob/master/Documentation/Artifacts.md#cartfile) for more details.

After modifying Cartfiles, you resolve and checkout the dependencies by calling `carthage update --no-build`.

At this point, a folder `Carthage/Checkouts` should appear in the working directory with one folder for each resolved dependency.

Next, you need to add the checkout projects to the main project:

* For dependencies included in the main target, create a top-level `Modules` group in the project and add the projects to it.
* For dependencies used by test target, a `Modules` group should be added to the existing `*Tests` group.

Finally, you need to include the dependency targets.

* If the main target is a framework, it should only link to the dependant projects
* If the main target is an app, it should both link *and* embed all the frameworks it uses

A few things to keep in mind when adding dependencies:

* You might find repos checked out that are not directly mentioned in the Cartfiles. These are child dependencies of the pulled in repo, and should also be added to the project.
* Ensure that all linked frameworks are embedded in app targets. When running the app on simulator the app will probably run fine even if the frameworks are not embedded, but this causes an immediate crash when running on device.
* If the projects you add links to other projects, they might be marked as missing. That’s to be expected and everything works fine as long as they’re all added to the main target correctly.

## Updating dependencies

You can update dependencies to the latest version by calling `carthage update --no-build`. Note that you might need to add new projects to the main project if any of the dependencies has added a new dependency of their own.

# Using projects with a dependency

When cloning projects, all you need to do is run `carthage checkout --no-build` and you’re ready to go.

It should be emphasised that you **must not** call `carthage update` when just checking it. That might change the resolved version of projects and result in an unexpect situation.

